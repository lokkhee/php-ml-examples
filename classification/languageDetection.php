<?php

declare(strict_types=1);

namespace PhpmlExamples;

include 'vendor/autoload.php';

use Phpml\Dataset\CsvDataset;
use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\ModelManager;
use Phpml\SupportVectorMachine\Kernel;

//-- Step 1
//-- Connection to mongodb database
//--
//-- Manager class (For mongodb.dll extension, old version of mongo.dll doesnt work)
$connection = new \MongoDB\Driver\Manager("mongodb://localhost:27017"); // Connects to localhost:27017

//-- Query Class ($filters , $options)
//-- Example:
//-- $filter = ['id' => 2];
//-- $options = ['projection' => ['_id' => 0],];
$query = new \MongoDB\Driver\Query([]);

$cursor = $connection->executeQuery('phpml.language', $query); // Change to own 'databasename.tablename'

//-- Output of the executeQuery will be object of MongoDB\Driver\Cursor class
//-- Convert cursor to Array and print result, 'senctence' and 'language' are the name of the data, change accordingly
//-- Step 2
//-- seperate sentences and language label data into 2 variable
foreach ($cursor as $r) {
    $convertedToHtmlEntities = mb_convert_encoding($r->sentence,'HTML-ENTITIES','UTF-8'); //Convert chinese character only -> "&#1234"
    $samples[] = $convertedToHtmlEntities;
    $languageArray[] = $r->language;
}

// print_r($samples);
// print_r($languageArray);

//-- Testing only --
// $cursor = $connection->executeQuery('phpml.languageEFI', $query);
//
// foreach ($cursor as $r) {
//     $samples[] = $r->sentence;
//     $languageArray[] = $r->language;
// }
// $samples[] = "\u7edd\u5bf9\u4e0d\u662f\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u4f60\u8ddf\u6211\u4e00\u8d77\u53bb\u5417\uff1f";
// $languageArray[] = "chinese";
// $samples[] = "\u4f60\u80fd\u80af\u5b9a\u5417\uff1f";
// $languageArray[] = "chinese";
// $samples[] = "\u660e\u5929\u6253\u7535\u8bdd\u7ed9\u6211\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u8bf7\u60a8\u8bf4\u5f97\u6162\u4e9b\u597d\u5417\uff1f";
// $languageArray[] = "chinese";
// $samples[] = "\u4e0d\u8981\u544a\u8bc9\u6211\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u795d\u4f60\u4e00\u5929\u8fc7\u5f97\u6109\u5feb\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u4ed6\u73b0\u5728\u5df2\u7ecf\u5728\u8def\u4e0a\u4e86\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u6211\u5bf9\u5979\u7740\u8ff7\u4e86\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u6211\u4e00\u4e2a\u4eba\u90fd\u4e0d\u8ba4\u8bc6\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u6211\u8ba4\u4e3a\u662f\u8fd9\u6837\u7684\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u8bf7\u7ed9\u6211\u4e00\u676f\u5496\u5561\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u6211\u4f1a\u60f3\u5ff5\u4f60\u7684\u3002";
// $languageArray[] = "chinese";
// $samples[] = "\u6211\u5f88\u65e0\u804a\u3002";
// $languageArray[] = "chinese";
// print_r($samples);
// print_r($languageArray);

//-- Step 3
//-- Convert the data and build up dictionary
$vectorizer = new TokenCountVectorizer(new WordTokenizer()); //To count the occurance of word in vocabulary
$tfIdfTransformer = new TfIdfTransformer();

$vectorizer->fit($samples); 
$vectorizer->transform($samples);

$tfIdfTransformer->fit($samples);
$tfIdfTransformer->transform($samples);

//-- Step 4
//-- Split the dataset and train the data
$dataset = new ArrayDataset($samples, $languageArray);

$randomSplit = new StratifiedRandomSplit($dataset, 0.3); //Stratified takes into account each label to prevent bias data

$classifier = new SVC(Kernel::RBF, 10000);
$classifier->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());

//-- Step 5
//-- This 3 line will save the Model, remember to comment after use to prevent overwrite
//-- Trained model will be saved for future use
$filepath = 'model/trainedHTML.dat';
$modelManager = new ModelManager();
$modelManager->saveToFile($classifier, $filepath);

//-- Step 6
//-- Prediction
$predictedLabels = $classifier->predict($randomSplit->getTestSamples());

//---------- This section below is used for checking-----

// print_r($randomSplit->getTestLabels());
// print_r($predictedLabels);

$testLabels = $randomSplit->getTestLabels(); //To get the label of the data correspond to the sample used

for ($i = 0; $i < count($testLabels); $i++) {
    //Show the data used
    echo $i . ' -> ' . $testLabels[$i] . '  predicted -> ' . $predictedLabels[$i] . ' ';
    if ($testLabels[$i] == $predictedLabels[$i]) {
        //If the test label and predicted label match
        echo ' pass' . PHP_EOL;
    } else {
        //Else
        echo ' fail' . PHP_EOL;
    }
}

//---------- This section above is used for checking-----


echo 'Accuracy: '.Accuracy::score($randomSplit->getTestLabels(), $predictedLabels);

