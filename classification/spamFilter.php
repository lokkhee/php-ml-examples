<?php

declare(strict_types=1);

namespace PhpmlExamples;

include 'vendor/autoload.php';

use Phpml\Dataset\CsvDataset;
use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\SupportVectorMachine\Kernel;

//temporarily alter the memory limit for such large dataset
ini_set('memory_limit', '-1');


echo 'Loading dataset...' . PHP_EOL;
$vectorizer = new TokenCountVectorizer(new WordTokenizer());
$tfIdfTransformer = new TfIdfTransformer();

$connection = new \MongoDB\Driver\Manager("mongodb://localhost:27017"); // Connects to localhost:27017

$query = new \MongoDB\Driver\Query([]);
$cursor = $connection->executeQuery('phpml.spam', $query); //Change to own 'Database name.table name'
echo 'Extracting samples ...' . PHP_EOL;
foreach ($cursor as $r) {
   //$r->first row(sample),$r->second row(class), also change to own table data name
    $samples[] = $r->Message;
    $classArray[] = $r->class;
}

echo 'Vectorizing samples ...' . PHP_EOL;
$vectorizer->fit($samples);
$vectorizer->transform($samples);

$tfIdfTransformer->fit($samples);
$tfIdfTransformer->transform($samples);

$dataset = new ArrayDataset($samples, $classArray);

$randomSplit = new StratifiedRandomSplit($dataset, 0.1);

echo 'Training model ...' . PHP_EOL;
$classifier = new SVC(Kernel::RBF, 1000);
$classifier->train($randomSplit->getTrainSamples(), $randomSplit->getTrainLabels());

echo 'Performing prediction ...' . PHP_EOL;
$predictedLabels = $classifier->predict($randomSplit->getTestSamples());

echo 'Accuracy: '.Accuracy::score($randomSplit->getTestLabels(), $predictedLabels) . PHP_EOL;
