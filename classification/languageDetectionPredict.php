<?php

declare(strict_types=1);

namespace PhpmlExamples;

include 'vendor/autoload.php';

use Phpml\Dataset\CsvDataset;
use Phpml\Dataset\ArrayDataset;
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\CrossValidation\StratifiedRandomSplit;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Metric\Accuracy;
use Phpml\Classification\SVC;
use Phpml\ModelManager;
use Phpml\SupportVectorMachine\Kernel;

//-- This file is use to restore the trined model and do prediction

$connection = new \MongoDB\Driver\Manager("mongodb://localhost:27017");

$query = new \MongoDB\Driver\Query([]);

$cursor = $connection->executeQuery('phpml.language', $query); // Change to own 'databasename.tablename'

//-- Convert cursor to Array and print result, 'senctence' and 'language' are the name of the data, change accordingly
foreach ($cursor as $r) {
    $convertedToHtmlEntities = mb_convert_encoding($r->sentence, 'HTML-ENTITIES', 'UTF-8');
    $samples[] = $convertedToHtmlEntities;
    $languageArray[] = $r->language;
}

$vectorizer = new TokenCountVectorizer(new WordTokenizer());
$tfIdfTransformer = new TfIdfTransformer();

$vectorizer->fit($samples);
$vectorizer->transform($samples);

//print_r($vectorizer->getVocabulary());

$tfIdfTransformer->fit($samples);
$tfIdfTransformer->transform($samples);

$testSamples = [
    "Vorrei prenotare un posto.", //italian 0
    "A sample of sentences to test the function of the program.", //english 1
    "How about going to the restaurant?",  //english 2
    "How about going to the nearby restaurant?",  //english 3
    "Vous devez faire une déclaration de perte.", //french 4
    "Posso avere i biglietti del cinema per favore?", //italian 5
    "\u8fd9\u4e2a\u662f\u4e2d\u6587\u53e5\u5b50\u3002",
    "Il n'avait pas la priorité.", //french 7
    "Quando c'è l'ultimo autobus?", //italian 8
    "\u4f60\u8981\u4e00\u8d77\u53bb\u770b\u7535\u5f71\u5417\uff1f",
    "\u4ed6\u73b0\u5728\u5df2\u7ecf\u5728\u8def\u4e0a\u4e86\u3002",
    "\u4f60\u8ddf\u6211\u4e00\u8d77\u53bb\u5417\uff1f",
    "\u9a6c\u6765\u897f\u4e9a\u534e\u8bed\u6216\u534e\u6587\u662f\u9a6c\u6765\u897f\u4e9a\u534e\u4eba\u5708\u4e2d\u666e\u904d\u4f7f\u7528\u7684\u4e00\u79cd\u542b\u6709\u5f53\u5730\u7279\u8272\u7684\u534e\u8bed",
    "Una frase molto lunga per rilevare la lingua", //italian 13
    "Des phrases très longues pour détecter la langue", //french 14
    "&#20182;&#29616;&#22312;&#24050;&#32463;&#22312;&#36335;&#19978;&#20102;&#12290;",
    "&#20320;&#36319;&#25105;&#19968;&#36215;&#21435;&#21527;&#65311;",
    "&#36825;&#37324;&#30340;&#19996;&#35199;&#37117;&#22909;&#22909;&#21507;&#65292;&#25105;&#22909;&#21916;&#27426;"
];

//$vectorizer->fit($testSamples);
$vectorizer->transform($testSamples);

//print_r($vectorizer->getVocabulary());

// $tfIdfTransformer->fit($testSamples);
$tfIdfTransformer->transform($testSamples);

$classifier = new SVC(Kernel::RBF, 10000);

//-- Retreive the trained model to use
$filepath = 'model/trainedHTML.dat';
$modelManager = new ModelManager();
$restoredClassifier = $modelManager->restoreFromFile($filepath);

$predicted = $restoredClassifier->predict($testSamples);

//$predicted = $restoredClassifier->predict($samples);

echo "\r\n中文" . PHP_EOL;
print_r($predicted);

//---------- This section below is only for checking, OPTIONAL-----
// // print_r( $randomSplit->getTestLabels());
// // print_r($predictedLabels);

// $testLabels = $randomSplit->getTestLabels(); //To get the label of the data correspond to the sample used

// for ($i = 0; $i < count($languageArray); $i++) {
//     //Show the data used
//     echo $i . ' -> ' . $languageArray[$i] . '  predicted -> ' . $predicted[$i] . ' ';
//     if ($languageArray[$i] == $predicted[$i]) {
//         //If the test label and predicted label match
//         echo ' pass' . PHP_EOL;
//     } else {
//         //Else
//         echo ' fail' . PHP_EOL;
//     }
// }
// //---------- This section above is only for checking, OPTIONAL-----


// echo 'Accuracy: ' . Accuracy::score($languageArray, $predicted);

